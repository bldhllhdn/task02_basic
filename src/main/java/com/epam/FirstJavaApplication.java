package com.epam;
import java.util.Scanner;

/**
 * @author Daryna Bilousova
 * @version 1.0
 */

public class FirstJavaApplication {
    /**
     * Main function of the program.
     * @param args required string array
     */
    public static void main(final String[] args) {
        Scanner in;

        int leftBorder;
        int rightBorder;
        while (true) {
            System.out.println("Enter the interval:");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                leftBorder = in.nextInt();
                rightBorder = in.nextInt();
                break;
            } else {
                System.out.println("Wrong number!");
            }
        }
        oddEvenFunction(leftBorder, rightBorder);
        fibonacciFunction(rightBorder);
    }

    /**
     * Method searches for and prints odd and even numbers of the interval.
     * @param first the first element of the interval
     * @param last the last element of the interval
     */
        private static void oddEvenFunction (final int first, final int last) {
        int sumEven = 0;
        int sumOdd = 0;
        System.out.print("\n");
        System.out.println("Odd numbers from start to the end of interval:");
        for (int value = first; value <= last; ++value) {
            if (value % 2 == 1) {
                sumOdd = sumOdd + value;
                System.out.print(value);
                System.out.print(" ");
            }
        }
        System.out.print("\n\nSum of odd numbers is " + sumOdd + "\n");
        System.out.print("\n");

        System.out.println("Even numbers from end to the start of interval:");
        for (int value = last; value >= first; --value) {
            if (value % 2 == 0) {
                sumEven = sumEven + value;
                System.out.print(value);
                System.out.print(" ");
            }
        }
        System.out.print("\n\nSum of even numbers is " + sumEven + "\n");
    }

    /**
     * Method counts Fibonacci numbers.
     * @param startPoint initial value
     */
    private static void fibonacciFunction(final int startPoint) {
        Scanner in;

        final int PERCENTS = 100;

        int fibonacci1 = 0;
        int fibonacci2 = 0;
        int fibonacci3 = 0;

        int fibonacciSize = 0;

        int fibonacciEven = 1;
        int fibonacciOdd = 1;

        double perEvenFibonacci = 0;
        double perOddFibonacci = 0;

        if (startPoint % 2 == 1) {
            fibonacci1 = startPoint;
            fibonacci2 = startPoint - 1;
        } else if (startPoint % 2 == 0) {
            fibonacci2 = startPoint;
            fibonacci1 = startPoint - 1;
        }

        while (true) {
            System.out.println("\nEnter the size of Fibonacci set:");
            in = new Scanner(System.in);
            if (in.hasNextInt()) {
                fibonacciSize = in.nextInt();
                break;
            } else {
                System.out.println("Wrong number!");
            }
        }
        System.out.print("\n");

        System.out.print(fibonacci1 + " ");
        System.out.print(fibonacci2 + " ");
        for (int i = 2; i < fibonacciSize; ++i) {
            fibonacci3 = fibonacci1 + fibonacci2;
            fibonacci1 = fibonacci2;
            fibonacci2 = fibonacci3;
            System.out.print(fibonacci3 + " ");
            if (fibonacci3 % 2 == 0) {
                fibonacciEven++;
            } else if (fibonacci3 % 2 == 1) {
                fibonacciOdd++;
            }
        }
        perEvenFibonacci = (double) (fibonacciEven * PERCENTS) / fibonacciSize;
        perOddFibonacci = (double) (fibonacciOdd * PERCENTS) / fibonacciSize;

        System.out.print("\n");
        System.out.println("Even fibonacci - " + perEvenFibonacci + '%');
        System.out.println("Odd fibonacci - " + perOddFibonacci + '%');
    }
}
